# How to use blackoutrugby on Web3

> [Airnode](https://api3.org/airnode) API Documentation

This API provides all game data for Blackout Rugby Manager. Available on [Steam](https://store.steampowered.com/app/1157960/Blackout_Rugby_Manager), and coming soon to mobile.

This doc is WiP. We haven't updated all endpoint info below yet, but will do so as soon as someone wants to use our Airnode API.

To talk to us for support, please join our [Discord](https://discord.gg/vayT2pV) server and chat in the `#api` channel.

Soon, this API will provide data for multiple sports games, using a header called `Blackout-Sport: e.g. football`. We will create a new URL for the new API.

**Home Page:** https://api.blackoutrugby.com/v1  
**Web2 Docs:** https://rugbyapi.docs.apiary.io/

## Call this Airnode API

Read the [Airnode developer documentation](https://docs.api3.org/d/call-an-airnode) to learn how to call Airnode APIs. You'll need the **Provider ID** to call any endpoint in this API.

**Provider ID:** 0xc1098196d3ebfa457b87b47ec3f76542910318e6bf3b980d6fb0c127fa573c64

**Provider XPub:** xpub661MyMwAqRbcEjvCBagqJgFLJKSyEPawe89VHv2Zp9AZhoPRF4NgCKcFfE1Aki45LZhffxzpUpBYxm1fo8dmcTbXoZWzb3pdpqy7pfkEusy

[Reserved Parameters](https://docs.api3.org/r/reserved-parameters) are used to control Airnode behavior and are available for all endpoints.

## Available on Networks:

> Find more information on each chain [Here](https://ethereum.org/en/developers/docs/networks/).

| Chain                                | Airnode RRP Contract                       |
| ------------------------------------ | ------------------------------------------ |
| Rinkeby                                 | 0xF9C39ec11055508BddA0Bc2a0234aBbbC09a3DeC                                 |

# Endpoints
1. [/managers/{id}](#0x1c7f4189ae4e571b9827494622e5353de9fecf3c64af5cb1dc30b71334ebb7ab)
2. [Get a manager by their email address](#0xee78e029a74b34edc96ad6687b460e62fd53a03bea18029255d426f2b80e69db)
3. [/managers/me](#0x8d2d91c3e442effbf696fc13fbfd26b12cfbab3a961315631a6390a9cfa7ca80)
4. [/managers](#0xc3a5fac553f532eda35f349d66b84ade62acb69a0b7f483407521e9f2f1c1b11)
5. [/email-preferences/{id}](#0x7edfbf490a5c356e5055a63c14ea0dd4e75a9ad2acf27e428652ec55bd8f282f)
6. [/email-preferences](#0x4cba6be68ec35aa36da921ac3b76bc97e3f2c28917fb9be28b4ebbcec5c3dee2)
7. [/clubs/{id}](#0xfe394ebf1d77ede0c4253b9353ebaac519d63de208fcabcae63708fc241d4f7b)
8. [/depth-charts/{id}](#0xdb658f42f66a8db0f3723bc4c39312cbdf7f4a946f688a7cb1033378c835bd64)
9. [/club-tokens/{id}](#0xa5bf2eb23dcb5d0c4902a48046aed190ea1ae5ab202c542eb0f15cc09638afdc)
10. [/club-history](#0x68a6db6af12ab4e35bb8b6e58460f68e3c5650976f49960d9f0bad972a820a43)
11. [/club-statistics/{id}](#0x2ad0a3f68bc9bc4b291fd714d0d63ddacb40f84943fb41a88ddca0e6eb5a83a4)
12. [Get club trophies](#0x6850fc9c52d55e45caa6145a825902c0d3431570806b6091815835f8844412a8)
13. [/club-trophies](#0xce921bb886d6a372134523d7a522ea8d004600dac692053e7b21d163cc033a2a)
14. [/inventories/{id}](#0x80ec6715d6b63e6fad776a04e155bd5640be31d1176272e1c5d7607c95a78958)
15. [/abandon-club](#0xc6a1156a507123bfb76f7e2bf4ca9237e2b7e738974d3276e9d53e3d110e612c)
16. [/sponsors/{id}](#0xa3b5403dfa1a684c6c4792c46caa197431fd66961c29588e7c27f500dac27bf4)
17. [sponsors](#0x4a1cad3c66fafe9bd8cf5225c0995d2324373532bb96673d5b90b85ef8129377)
18. [/players/{id}](#0xc0edea8d76e1f72b479a321f6e4bea21fe10fcfb86e8b3b59a3a06ef3d765450)
19. [/player-history](#0x6603ff1ebcfd785cd2ad07c4df910bfbe6917b18418da4439fb8b4f7e2fdbf3f)
20. [/transfers-history](#0x1130db6db1d8dc01195987b167e84b87560c68e005950782e4109203220fa91c)
21. [/player-contract-negotiations/{id}](#0x2dc23d7f68030d8a8f53984c549e0ef0ca99504582c7dbce5e12b47f04a617a0)
22. [/player-contract-negotiations](#0x5043840d194453d77e95d7b103d12162ba2467a1cc363da5f2eb7e5fa7ae2787)
23. [/player-statistics/{id}](#0xb742b8708244bfa25038d800c46d56e5370cb98017a0e50de9e3444e61eb4edd)
24. [/first-star-player](#0xe0213ef40e0ac5a73f9820efa200daa1dbc2097993cb71c6d875d6575b396453)
25. [/academy/{id}](#0x17ba703240970b576a5f3a2fb9082275fd1e7187f2a70a813a71f14b215f8315)
26. [/academy](#0xd5e79363474d37f3f250d1cb0d70053c0398a44eae5b3d080e66f0fedfe1ffc1)
27. [/scouting-assignments](#0x3ef0ec8ac75882cb3a5b317f407cca77dedfab9204b8a8efc87368e01a70815f)
28. [/scouting-assignments/{id}](#0x7cbdc23a350a6f147e2bea2d7f617eb0da59f7b35cb16f0fd6fcf2b820001ef2)
29. [/lineups/{id}](#0xdb83062bc3c59e5f4c96aab8254b1d9a17ae463851466b262a7ef0b637103ce7)
30. [Get all lineups in a club](#0x6b72d32d96a464093284b364488e76c435ef73c17e6052bf6ee80c0f05029261)
31. [/lineups](#0x04745d699e2a7c1f28c8b1e2ed3a4ba4e38b10eacb993f01ef93b0d590d4cbd7)
32. [/fixtures/{id}](#0x5ef972d267d2114cb1aeacdcee68bf2f0572dc5d9846ab35b345b108394dcea7)
33. [/fixtures](#0xce94aadfe7775e102807bc471306d12188925ab2ac00a0eca8745ddbb885feea)
34. [/matches](#0xbb14642cc0d60c11c97130c2872e76bdef2be7b2e65604366c4929ddcabb8f12)
35. [/match-statistics/{id}](#0x3a6b75cfd25dda7bc5e05ea0ca254400a6b4409260cbc1f3b74bb0c53aa898f4)
36. [/nutrition-plans](#0x2bb501dc363dfaa387dcaf9404568163f486a7afc39a9f4fca81585be6e98333)
37. [/training-sessions](#0x4a06e7fc4be6d14729b2469903c0beb86a68db0d371a6c78fcc9b5ff2ee2017a)
38. [/training-sessions/{id}](#0xaccd8fab335e3ecb3294d9ccac27cb5d7ef921c7b3c08e8fe70c94c84053bfca)
39. [Get Club Training Regimes](#0xa8c37f0a42c772354dc84e7733b0d6cfb42ffc6bcc33db6b670707ff449ddbd4)
40. [/training-regimes](#0xac0bfa88332410f787fb610e6265896a33327a1e4c58dc49282f9ba79fb6a36c)
41. [/training](#0xb552906bb6d3255506f4f0f736fd9b0846d25ff1a615b3be40b2b27efa293b54)
42. [/training-reports](#0x16c288313560172ca1678f6b71bfd8355f460b4f68392968e155c21dbbf545c3)
43. [/countries/{id}](#0x1458e36dcad0a835cdf6d73f1d4c6033e226b4c7b9134f7a5e0e47ee9c9567bc)
44. [/countries](#0xc590177dbf50a0508e946f4e3734c0d144b0fd9193eff90f563b8cc5ae90b8a1)
45. [/game-plans/{id}](#0x30787da5e0d06486308582d967e1ae3ca4745f6067d7c56f6dc78239b5f532f1)
46. [Get all GamePlans in a club](#0x5563e65bb9940124a2b497c28c4e16d8ec53ee9fb97b151e68fd9adb523e20ab)
47. [/game-plans](#0xcaae272889d8a28de5a6ed949a013539299f90ecc25509e504f04ec78a78dc5d)
48. [/leagues/{id}](#0xef0123655dc9963a8446e460bb59799453d4ee78b0b17e4232323fd7089eabd0)
49. [/ladder-clubs/{id}](#0xb54acc76986e04f54337a259c976c658da5d617ab5856b37dd7f7596563b8500)
50. [Get global ladder for club](#0x4cc547a0c3fc2d745d556d27e4e04a72e364e93fd5bb481cd6eecf2a294f27bb)
51. [/ladder-clubs](#0xa4796787b677900f0f2c8aaeff3840849b1437503bea1c3c91464643a32502d2)
52. [/ladder-settings/{id}](#0x1b13b5457774f0a2819606c3d1dafb97e3b57180e9cd1e5def35e1ad0bed77c5)
53. [Planned Friendlies](#0x2196a3512a3e9db4e42392f91177e090cf30babd8f305f754e89200dc24cdc06)
54. [/friendlies](#0x2b024e9d4919d2f6bda8f8142050a083c90a5d37b26153eaa93946080d754522)
55. [/buildings](#0xdaecf46b4a810fa962625c6774f63dbeecb5f55b98102b80d72ec63241c31005)
56. [/buildings/{id}](#0xff7d81eab42a2e97ab88d6190bb70686120da3501c5b83da75a4b9571c9488bb)
57. [/grounds/{id}](#0x91cdc4f0bb0bcae82c1d46c61af1aaedb28ecc410ca8af78ecbf93691920e039)
58. [/grounds](#0xba3cde6c5f92a0ac3f3824dff739b8daf5701e4f3481e50a75f96e051e929501)
59. [/amenities](#0x464d2cd28436cb6435ddbd3df954e2b89c874bac790094a7da608db9439002fb)
60. [/amenities/{id}](#0x8af5733f4739f8dcf3173d7869b7c03cc6727a47b38234882fb97f897e6cb9f2)
61. [/tech-trees](#0x7877458400359bbb87fa653fdec522b9965308cd8dd950dc47f317f65dea3510)
62. [/tech-trees/{id}](#0xcb8d978972872411f1a7db1d85d4e4104d836e7b2ec603201699d4ebfe8a7e24)
63. [/treatment-rooms/{id}](#0x43cd70585245b2cbd5d09dc691fb2256ce5161abca299c6d8bd43fc79a0bd5f9)
64. [/card-prototypes](#0xf75271f692b0de1c8c0124531e6816d59a66e2663bd0e9817cc98de3f93028a6)
65. [card-prototypes/{id}](#0x9581c56ec68d0d7698c0c2bb1f61601e69a33fcfe8ec5a98e569bc609ccbd270)
66. [/cards/{id}](#0x307e41cdddc82a522c8cc925ba07b7f4111586c7db66f9252acdec5fe60baf1a)
67. [/notifications/me](#0xc901f4f7c70d4a2dc5622bf8e1e20b0830ee946485db5d876cfe0e18d983eaf4)
68. [/notifications/{id}](#0x161b5a1933c66b56a4a2a21b63fa58ca29bab64f2178097d41287f8e64720505)
69. [/help](#0x8e0f0d4cf7c5b8523451d426cec5307d60fd8c2b95b4641bb84977a9f54a9fc0)
70. [/help/{id}](#0x3a36a91c3c05c9c855783071a7492233e0e5792c7c8f814c7089ca03987790b3)
71. [/settings](#0x5bfb66fd7306ab38293ed0c08eef25d138ec94fa3d191a2a2d427a665d842769)
72. [/settings/{id}](#0xb7a835962ac70245672d8228cc091d1cb1272e6fb42b7c0b12b3f72162c49cc5)
73. [/weather-events/{id}](#0x80bad01bfe91cc45d80a77cc0c83a7770cf4224ef0771dac9baab4b22ba2e701)
74. [/store](#0x829efc1f39acc97f86760e71d9d01ee056662decdabe8233b68588b035445f7c)
75. [/campaigns](#0x1e659165510c77911058cfa8a20165dd18005320c468bfc439766f0e50e696f7)
76. [/campaigns/{id}](#0x712eb2400eb26e52be2e472f2fce1e025c2f0945a97060dfd1b341892b2d2e4f)
77. [/club-campaigns](#0x5bef9356a16839573ad32e925fe4ef43766ea4dfc7be55d8281b35a524e96d5b)
78. [/invite-a-friend](#0x7367581a066b505ac7e962d07d4c36fa433aeb8b896d0f5fcae50ded86347c48)
79. [/social/{id}](#0x927545f0f8ae9544d31994cf14624fb734533b936e7349a790c8a87218acbe21)
80. [Get social by managerId](#0x028325a5e836ccddbf06de3d43b5aed45457017db1ce7ccd8fb6060835a714da)
81. [/social](#0x9f4a4bfa2a6608f301079d5cfaa7bf89f0ef03e7b2e67d84c221cf10509d0ef4)
82. [/subscriptions/{id}](#0xf421e49095a06121fe3c7cf4681aebfbc52d6aab1e68679aedae4465679f7051)
83. [Get all subscriptions for a user](#0x230ecae3f32982de44e83c418603d5dc3320ffb4fd3ea3381edeb834546c92e0)
84. [/subscriptions](#0x277bdb41377bede649c4b4649f72b4e1b9e249f2cc9cab278f63ce0ff5f19e6a)
85. [Assign Tokens](#0xee465cc1dd4b64a64d49aa0c709371bb93ef601139b2132361c25d3a8d2d7a86)
86. [/assign-tokens](#0x6d8726ac8120e5629ce81b9f3a24f31d71dbb0b7bb8e897722e08a87ef9b32af)
87. [Get Assistant Manager Settings](#0xefaddf4ccf2857c64d963ef29661a51b1bfd1bc32b49b9d5b1bfa4e55ca59597)
88. [/assistant-manager](#0xb702dd9fc5d774a8349a0c06d60b773d142b0f33602367224c680f1076c9f1f9)
89. [/hall-of-fame/{id}](#0xb0002aa8688c8c699f82228f179bbb9827c080788896f70cf197bde46702a577)
90. [/hall-of-fame](#0xadeaadc99b8996230e3b2b19f7cb709323cd62a57b3f5d1a87aa348720d13c8f)
91. [/top/{id}](#0x5e7288753b83ef877a736c6840d20c419acc454b0b628f6dda772cf2995a4120)
92. [List all Unions](#0xfc8b049f92729eb1370b0fd8438c3bc6a21d72e3293a5c4aef1aeaa405527fbd)
93. [/unions](#0xace6db3b78ba87d787f17f1bc6c5d67dd4693c3ee70597f3d9bda27dfe602e7f)
94. [/chat-messages](#0x4232ea268e1b214f60f5b4e8f66a91aa882e797380647f164f9fffa5e3eb8d87)
95. [/chat-messages/{id}](#0x8234f51bd417dbbc989a420a25c70fa2219a8fe7c8c5248ac354988a612fbec4)
96. [/union-history/{id}](#0x3b0a621aa24e342a4a1eae6d84288e1845fe08d2716b68bf74b4ff4b955d6aa1)
97. [/union-challenge-packs](#0x6cc166c7c43fac2f014dd7ff4bb9313a1abfe5c0ee05d08006ad889b35c8fd1c)
98. [/union-challenge-packs/{id}](#0x70b0877cd8e105ffe088d033681fc18b99d95e3770dc7844d3a0bb9253648821)
99. [List chats for a manager](#0x4b6bb673477fb8655b387c57dabd9e233fe931ea421dc2c72dba57cc643fedb1)
100. [/chats/{id}](#0xa41dd6906d145acbd956e787b72b3c04a9df6249adf03ef941c5f32ba6d872b4)
101. [/chats](#0xe66ced51b1d9fc1b156f55eee11f2785b8263b4de4ea5ac36cdc1ba48611931f)
---
## /managers/{id} <a name="0x1c7f4189ae4e571b9827494622e5353de9fecf3c64af5cb1dc30b71334ebb7ab"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x1c7f4189ae4e571b9827494622e5353de9fecf3c64af5cb1dc30b71334ebb7ab

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
id		// Parameter Description...
include		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## Get a manager by their email address <a name="0xee78e029a74b34edc96ad6687b460e62fd53a03bea18029255d426f2b80e69db"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0xee78e029a74b34edc96ad6687b460e62fd53a03bea18029255d426f2b80e69db

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
filter[email]		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /managers/me <a name="0x8d2d91c3e442effbf696fc13fbfd26b12cfbab3a961315631a6390a9cfa7ca80"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x8d2d91c3e442effbf696fc13fbfd26b12cfbab3a961315631a6390a9cfa7ca80

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
None
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /managers <a name="0xc3a5fac553f532eda35f349d66b84ade62acb69a0b7f483407521e9f2f1c1b11"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0xc3a5fac553f532eda35f349d66b84ade62acb69a0b7f483407521e9f2f1c1b11

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
clubCountry		// Parameter Description...
clubName		// Parameter Description...
email		// Parameter Description...
facebookToken		// Parameter Description...
password		// Parameter Description...
referralCode		// Parameter Description...
username		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /email-preferences/{id} <a name="0x7edfbf490a5c356e5055a63c14ea0dd4e75a9ad2acf27e428652ec55bd8f282f"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x7edfbf490a5c356e5055a63c14ea0dd4e75a9ad2acf27e428652ec55bd8f282f

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
id		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /email-preferences <a name="0x4cba6be68ec35aa36da921ac3b76bc97e3f2c28917fb9be28b4ebbcec5c3dee2"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x4cba6be68ec35aa36da921ac3b76bc97e3f2c28917fb9be28b4ebbcec5c3dee2

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
email		// Parameter Description...
optOut		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /clubs/{id} <a name="0xfe394ebf1d77ede0c4253b9353ebaac519d63de208fcabcae63708fc241d4f7b"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0xfe394ebf1d77ede0c4253b9353ebaac519d63de208fcabcae63708fc241d4f7b

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
filter[manager]		// Parameter Description...
id		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /depth-charts/{id} <a name="0xdb658f42f66a8db0f3723bc4c39312cbdf7f4a946f688a7cb1033378c835bd64"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0xdb658f42f66a8db0f3723bc4c39312cbdf7f4a946f688a7cb1033378c835bd64

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
id		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /club-tokens/{id} <a name="0xa5bf2eb23dcb5d0c4902a48046aed190ea1ae5ab202c542eb0f15cc09638afdc"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0xa5bf2eb23dcb5d0c4902a48046aed190ea1ae5ab202c542eb0f15cc09638afdc

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
filter[club]		// Parameter Description...
id		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /club-history <a name="0x68a6db6af12ab4e35bb8b6e58460f68e3c5650976f49960d9f0bad972a820a43"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x68a6db6af12ab4e35bb8b6e58460f68e3c5650976f49960d9f0bad972a820a43

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
filter[Club]		// Parameter Description...
limit		// Parameter Description...
season		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /club-statistics/{id} <a name="0x2ad0a3f68bc9bc4b291fd714d0d63ddacb40f84943fb41a88ddca0e6eb5a83a4"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x2ad0a3f68bc9bc4b291fd714d0d63ddacb40f84943fb41a88ddca0e6eb5a83a4

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
id		// Parameter Description...
season		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## Get club trophies <a name="0x6850fc9c52d55e45caa6145a825902c0d3431570806b6091815835f8844412a8"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x6850fc9c52d55e45caa6145a825902c0d3431570806b6091815835f8844412a8

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
filter[club]		// Parameter Description...
highlights		// Parameter Description...
sort		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /club-trophies <a name="0xce921bb886d6a372134523d7a522ea8d004600dac692053e7b21d163cc033a2a"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0xce921bb886d6a372134523d7a522ea8d004600dac692053e7b21d163cc033a2a

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
club		// Parameter Description...
purchaseHighlight		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /inventories/{id} <a name="0x80ec6715d6b63e6fad776a04e155bd5640be31d1176272e1c5d7607c95a78958"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x80ec6715d6b63e6fad776a04e155bd5640be31d1176272e1c5d7607c95a78958

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
club		// Parameter Description...
id		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /abandon-club <a name="0xc6a1156a507123bfb76f7e2bf4ca9237e2b7e738974d3276e9d53e3d110e612c"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0xc6a1156a507123bfb76f7e2bf4ca9237e2b7e738974d3276e9d53e3d110e612c

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
cancel		// Parameter Description...
club		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /sponsors/{id} <a name="0xa3b5403dfa1a684c6c4792c46caa197431fd66961c29588e7c27f500dac27bf4"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0xa3b5403dfa1a684c6c4792c46caa197431fd66961c29588e7c27f500dac27bf4

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
id		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## sponsors <a name="0x4a1cad3c66fafe9bd8cf5225c0995d2324373532bb96673d5b90b85ef8129377"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x4a1cad3c66fafe9bd8cf5225c0995d2324373532bb96673d5b90b85ef8129377

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
sort		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /players/{id} <a name="0xc0edea8d76e1f72b479a321f6e4bea21fe10fcfb86e8b3b59a3a06ef3d765450"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0xc0edea8d76e1f72b479a321f6e4bea21fe10fcfb86e8b3b59a3a06ef3d765450

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
filter[club]		// Parameter Description...
id		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /player-history <a name="0x6603ff1ebcfd785cd2ad07c4df910bfbe6917b18418da4439fb8b4f7e2fdbf3f"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x6603ff1ebcfd785cd2ad07c4df910bfbe6917b18418da4439fb8b4f7e2fdbf3f

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
filter[Player]		// Parameter Description...
limit		// Parameter Description...
season		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /transfers-history <a name="0x1130db6db1d8dc01195987b167e84b87560c68e005950782e4109203220fa91c"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x1130db6db1d8dc01195987b167e84b87560c68e005950782e4109203220fa91c

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
filter[Player]		// Parameter Description...
filter[SellerClub]		// Parameter Description...
limit		// Parameter Description...
season		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /player-contract-negotiations/{id} <a name="0x2dc23d7f68030d8a8f53984c549e0ef0ca99504582c7dbce5e12b47f04a617a0"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x2dc23d7f68030d8a8f53984c549e0ef0ca99504582c7dbce5e12b47f04a617a0

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
id		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /player-contract-negotiations <a name="0x5043840d194453d77e95d7b103d12162ba2467a1cc363da5f2eb7e5fa7ae2787"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x5043840d194453d77e95d7b103d12162ba2467a1cc363da5f2eb7e5fa7ae2787

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
player		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /player-statistics/{id} <a name="0xb742b8708244bfa25038d800c46d56e5370cb98017a0e50de9e3444e61eb4edd"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0xb742b8708244bfa25038d800c46d56e5370cb98017a0e50de9e3444e61eb4edd

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
id		// Parameter Description...
season		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /first-star-player <a name="0xe0213ef40e0ac5a73f9820efa200daa1dbc2097993cb71c6d875d6575b396453"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0xe0213ef40e0ac5a73f9820efa200daa1dbc2097993cb71c6d875d6575b396453

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
None
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /academy/{id} <a name="0x17ba703240970b576a5f3a2fb9082275fd1e7187f2a70a813a71f14b215f8315"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x17ba703240970b576a5f3a2fb9082275fd1e7187f2a70a813a71f14b215f8315

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
id		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /academy <a name="0xd5e79363474d37f3f250d1cb0d70053c0398a44eae5b3d080e66f0fedfe1ffc1"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0xd5e79363474d37f3f250d1cb0d70053c0398a44eae5b3d080e66f0fedfe1ffc1

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
assignmentLevel		// Parameter Description...
beginDevelopment		// Parameter Description...
club		// Parameter Description...
promote		// Parameter Description...
rerun		// Parameter Description...
starsBalance		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /scouting-assignments <a name="0x3ef0ec8ac75882cb3a5b317f407cca77dedfab9204b8a8efc87368e01a70815f"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x3ef0ec8ac75882cb3a5b317f407cca77dedfab9204b8a8efc87368e01a70815f

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
None
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /scouting-assignments/{id} <a name="0x7cbdc23a350a6f147e2bea2d7f617eb0da59f7b35cb16f0fd6fcf2b820001ef2"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x7cbdc23a350a6f147e2bea2d7f617eb0da59f7b35cb16f0fd6fcf2b820001ef2

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
id		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /lineups/{id} <a name="0xdb83062bc3c59e5f4c96aab8254b1d9a17ae463851466b262a7ef0b637103ce7"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0xdb83062bc3c59e5f4c96aab8254b1d9a17ae463851466b262a7ef0b637103ce7

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
id		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## Get all lineups in a club <a name="0x6b72d32d96a464093284b364488e76c435ef73c17e6052bf6ee80c0f05029261"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x6b72d32d96a464093284b364488e76c435ef73c17e6052bf6ee80c0f05029261

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
filter[club]		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /lineups <a name="0x04745d699e2a7c1f28c8b1e2ed3a4ba4e38b10eacb993f01ef93b0d590d4cbd7"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x04745d699e2a7c1f28c8b1e2ed3a4ba4e38b10eacb993f01ef93b0d590d4cbd7

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
club		// Parameter Description...
lineup		// Parameter Description...
name		// Parameter Description...
setAsDefault		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /fixtures/{id} <a name="0x5ef972d267d2114cb1aeacdcee68bf2f0572dc5d9846ab35b345b108394dcea7"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x5ef972d267d2114cb1aeacdcee68bf2f0572dc5d9846ab35b345b108394dcea7

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
id		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /fixtures <a name="0xce94aadfe7775e102807bc471306d12188925ab2ac00a0eca8745ddbb885feea"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0xce94aadfe7775e102807bc471306d12188925ab2ac00a0eca8745ddbb885feea

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
club		// Parameter Description...
filter[league]		// Parameter Description...
maxDate		// Parameter Description...
minDate		// Parameter Description...
numFixtures		// Parameter Description...
past		// Parameter Description...
upcoming		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /matches <a name="0xbb14642cc0d60c11c97130c2872e76bdef2be7b2e65604366c4929ddcabb8f12"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0xbb14642cc0d60c11c97130c2872e76bdef2be7b2e65604366c4929ddcabb8f12

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
filter[fixture]		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /match-statistics/{id} <a name="0x3a6b75cfd25dda7bc5e05ea0ca254400a6b4409260cbc1f3b74bb0c53aa898f4"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x3a6b75cfd25dda7bc5e05ea0ca254400a6b4409260cbc1f3b74bb0c53aa898f4

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
id		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /nutrition-plans <a name="0x2bb501dc363dfaa387dcaf9404568163f486a7afc39a9f4fca81585be6e98333"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x2bb501dc363dfaa387dcaf9404568163f486a7afc39a9f4fca81585be6e98333

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
None
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /training-sessions <a name="0x4a06e7fc4be6d14729b2469903c0beb86a68db0d371a6c78fcc9b5ff2ee2017a"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x4a06e7fc4be6d14729b2469903c0beb86a68db0d371a6c78fcc9b5ff2ee2017a

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
None
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /training-sessions/{id} <a name="0xaccd8fab335e3ecb3294d9ccac27cb5d7ef921c7b3c08e8fe70c94c84053bfca"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0xaccd8fab335e3ecb3294d9ccac27cb5d7ef921c7b3c08e8fe70c94c84053bfca

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
id		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## Get Club Training Regimes <a name="0xa8c37f0a42c772354dc84e7733b0d6cfb42ffc6bcc33db6b670707ff449ddbd4"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0xa8c37f0a42c772354dc84e7733b0d6cfb42ffc6bcc33db6b670707ff449ddbd4

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
filter[club]		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /training-regimes <a name="0xac0bfa88332410f787fb610e6265896a33327a1e4c58dc49282f9ba79fb6a36c"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0xac0bfa88332410f787fb610e6265896a33327a1e4c58dc49282f9ba79fb6a36c

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
club		// Parameter Description...
image		// Parameter Description...
players		// Parameter Description...
sessions		// Parameter Description...
title		// Parameter Description...
type		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /training <a name="0xb552906bb6d3255506f4f0f736fd9b0846d25ff1a615b3be40b2b27efa293b54"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0xb552906bb6d3255506f4f0f736fd9b0846d25ff1a615b3be40b2b27efa293b54

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
club		// Parameter Description...
type		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /training-reports <a name="0x16c288313560172ca1678f6b71bfd8355f460b4f68392968e155c21dbbf545c3"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x16c288313560172ca1678f6b71bfd8355f460b4f68392968e155c21dbbf545c3

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
filter[club]		// Parameter Description...
last		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /countries/{id} <a name="0x1458e36dcad0a835cdf6d73f1d4c6033e226b4c7b9134f7a5e0e47ee9c9567bc"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x1458e36dcad0a835cdf6d73f1d4c6033e226b4c7b9134f7a5e0e47ee9c9567bc

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
id		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /countries <a name="0xc590177dbf50a0508e946f4e3734c0d144b0fd9193eff90f563b8cc5ae90b8a1"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0xc590177dbf50a0508e946f4e3734c0d144b0fd9193eff90f563b8cc5ae90b8a1

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
ids		// Parameter Description...
sort		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /game-plans/{id} <a name="0x30787da5e0d06486308582d967e1ae3ca4745f6067d7c56f6dc78239b5f532f1"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x30787da5e0d06486308582d967e1ae3ca4745f6067d7c56f6dc78239b5f532f1

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
id		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## Get all GamePlans in a club <a name="0x5563e65bb9940124a2b497c28c4e16d8ec53ee9fb97b151e68fd9adb523e20ab"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x5563e65bb9940124a2b497c28c4e16d8ec53ee9fb97b151e68fd9adb523e20ab

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
filter[club]		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /game-plans <a name="0xcaae272889d8a28de5a6ed949a013539299f90ecc25509e504f04ec78a78dc5d"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0xcaae272889d8a28de5a6ed949a013539299f90ecc25509e504f04ec78a78dc5d

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
club		// Parameter Description...
gamePlan		// Parameter Description...
name		// Parameter Description...
setAsDefault		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /leagues/{id} <a name="0xef0123655dc9963a8446e460bb59799453d4ee78b0b17e4232323fd7089eabd0"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0xef0123655dc9963a8446e460bb59799453d4ee78b0b17e4232323fd7089eabd0

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
id		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /ladder-clubs/{id} <a name="0xb54acc76986e04f54337a259c976c658da5d617ab5856b37dd7f7596563b8500"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0xb54acc76986e04f54337a259c976c658da5d617ab5856b37dd7f7596563b8500

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
club		// Parameter Description...
id		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## Get global ladder for club <a name="0x4cc547a0c3fc2d745d556d27e4e04a72e364e93fd5bb481cd6eecf2a294f27bb"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x4cc547a0c3fc2d745d556d27e4e04a72e364e93fd5bb481cd6eecf2a294f27bb

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
club		// Parameter Description...
ladder		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /ladder-clubs <a name="0xa4796787b677900f0f2c8aaeff3840849b1437503bea1c3c91464643a32502d2"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0xa4796787b677900f0f2c8aaeff3840849b1437503bea1c3c91464643a32502d2

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
None
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /ladder-settings/{id} <a name="0x1b13b5457774f0a2819606c3d1dafb97e3b57180e9cd1e5def35e1ad0bed77c5"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x1b13b5457774f0a2819606c3d1dafb97e3b57180e9cd1e5def35e1ad0bed77c5

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
id		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## Planned Friendlies <a name="0x2196a3512a3e9db4e42392f91177e090cf30babd8f305f754e89200dc24cdc06"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x2196a3512a3e9db4e42392f91177e090cf30babd8f305f754e89200dc24cdc06

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
filter[initiatorClub]		// Parameter Description...
filter[opponentClub]		// Parameter Description...
planned		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /friendlies <a name="0x2b024e9d4919d2f6bda8f8142050a083c90a5d37b26153eaa93946080d754522"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x2b024e9d4919d2f6bda8f8142050a083c90a5d37b26153eaa93946080d754522

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
homeGame		// Parameter Description...
initiatorClub		// Parameter Description...
kickOff		// Parameter Description...
opponentClub		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /buildings <a name="0xdaecf46b4a810fa962625c6774f63dbeecb5f55b98102b80d72ec63241c31005"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0xdaecf46b4a810fa962625c6774f63dbeecb5f55b98102b80d72ec63241c31005

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
None
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /buildings/{id} <a name="0xff7d81eab42a2e97ab88d6190bb70686120da3501c5b83da75a4b9571c9488bb"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0xff7d81eab42a2e97ab88d6190bb70686120da3501c5b83da75a4b9571c9488bb

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
id		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /grounds/{id} <a name="0x91cdc4f0bb0bcae82c1d46c61af1aaedb28ecc410ca8af78ecbf93691920e039"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x91cdc4f0bb0bcae82c1d46c61af1aaedb28ecc410ca8af78ecbf93691920e039

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
id		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /grounds <a name="0xba3cde6c5f92a0ac3f3824dff739b8daf5701e4f3481e50a75f96e051e929501"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0xba3cde6c5f92a0ac3f3824dff739b8daf5701e4f3481e50a75f96e051e929501

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
None
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /amenities <a name="0x464d2cd28436cb6435ddbd3df954e2b89c874bac790094a7da608db9439002fb"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x464d2cd28436cb6435ddbd3df954e2b89c874bac790094a7da608db9439002fb

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
None
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /amenities/{id} <a name="0x8af5733f4739f8dcf3173d7869b7c03cc6727a47b38234882fb97f897e6cb9f2"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x8af5733f4739f8dcf3173d7869b7c03cc6727a47b38234882fb97f897e6cb9f2

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
id		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /tech-trees <a name="0x7877458400359bbb87fa653fdec522b9965308cd8dd950dc47f317f65dea3510"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x7877458400359bbb87fa653fdec522b9965308cd8dd950dc47f317f65dea3510

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
None
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /tech-trees/{id} <a name="0xcb8d978972872411f1a7db1d85d4e4104d836e7b2ec603201699d4ebfe8a7e24"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0xcb8d978972872411f1a7db1d85d4e4104d836e7b2ec603201699d4ebfe8a7e24

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
id		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /treatment-rooms/{id} <a name="0x43cd70585245b2cbd5d09dc691fb2256ce5161abca299c6d8bd43fc79a0bd5f9"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x43cd70585245b2cbd5d09dc691fb2256ce5161abca299c6d8bd43fc79a0bd5f9

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
id		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /card-prototypes <a name="0xf75271f692b0de1c8c0124531e6816d59a66e2663bd0e9817cc98de3f93028a6"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0xf75271f692b0de1c8c0124531e6816d59a66e2663bd0e9817cc98de3f93028a6

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
None
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## card-prototypes/{id} <a name="0x9581c56ec68d0d7698c0c2bb1f61601e69a33fcfe8ec5a98e569bc609ccbd270"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x9581c56ec68d0d7698c0c2bb1f61601e69a33fcfe8ec5a98e569bc609ccbd270

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
id		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /cards/{id} <a name="0x307e41cdddc82a522c8cc925ba07b7f4111586c7db66f9252acdec5fe60baf1a"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x307e41cdddc82a522c8cc925ba07b7f4111586c7db66f9252acdec5fe60baf1a

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
id		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /notifications/me <a name="0xc901f4f7c70d4a2dc5622bf8e1e20b0830ee946485db5d876cfe0e18d983eaf4"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0xc901f4f7c70d4a2dc5622bf8e1e20b0830ee946485db5d876cfe0e18d983eaf4

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
None
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /notifications/{id} <a name="0x161b5a1933c66b56a4a2a21b63fa58ca29bab64f2178097d41287f8e64720505"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x161b5a1933c66b56a4a2a21b63fa58ca29bab64f2178097d41287f8e64720505

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
id		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /help <a name="0x8e0f0d4cf7c5b8523451d426cec5307d60fd8c2b95b4641bb84977a9f54a9fc0"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x8e0f0d4cf7c5b8523451d426cec5307d60fd8c2b95b4641bb84977a9f54a9fc0

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
None
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /help/{id} <a name="0x3a36a91c3c05c9c855783071a7492233e0e5792c7c8f814c7089ca03987790b3"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x3a36a91c3c05c9c855783071a7492233e0e5792c7c8f814c7089ca03987790b3

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
id		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /settings <a name="0x5bfb66fd7306ab38293ed0c08eef25d138ec94fa3d191a2a2d427a665d842769"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x5bfb66fd7306ab38293ed0c08eef25d138ec94fa3d191a2a2d427a665d842769

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
None
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /settings/{id} <a name="0xb7a835962ac70245672d8228cc091d1cb1272e6fb42b7c0b12b3f72162c49cc5"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0xb7a835962ac70245672d8228cc091d1cb1272e6fb42b7c0b12b3f72162c49cc5

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
id		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /weather-events/{id} <a name="0x80bad01bfe91cc45d80a77cc0c83a7770cf4224ef0771dac9baab4b22ba2e701"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x80bad01bfe91cc45d80a77cc0c83a7770cf4224ef0771dac9baab4b22ba2e701

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
id		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /store <a name="0x829efc1f39acc97f86760e71d9d01ee056662decdabe8233b68588b035445f7c"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x829efc1f39acc97f86760e71d9d01ee056662decdabe8233b68588b035445f7c

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
club		// Parameter Description...
product		// Parameter Description...
transactionInfo		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /campaigns <a name="0x1e659165510c77911058cfa8a20165dd18005320c468bfc439766f0e50e696f7"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x1e659165510c77911058cfa8a20165dd18005320c468bfc439766f0e50e696f7

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
None
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /campaigns/{id} <a name="0x712eb2400eb26e52be2e472f2fce1e025c2f0945a97060dfd1b341892b2d2e4f"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x712eb2400eb26e52be2e472f2fce1e025c2f0945a97060dfd1b341892b2d2e4f

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
id		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /club-campaigns <a name="0x5bef9356a16839573ad32e925fe4ef43766ea4dfc7be55d8281b35a524e96d5b"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x5bef9356a16839573ad32e925fe4ef43766ea4dfc7be55d8281b35a524e96d5b

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
filter[club]		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /invite-a-friend <a name="0x7367581a066b505ac7e962d07d4c36fa433aeb8b896d0f5fcae50ded86347c48"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x7367581a066b505ac7e962d07d4c36fa433aeb8b896d0f5fcae50ded86347c48

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
email		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /social/{id} <a name="0x927545f0f8ae9544d31994cf14624fb734533b936e7349a790c8a87218acbe21"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x927545f0f8ae9544d31994cf14624fb734533b936e7349a790c8a87218acbe21

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
id		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## Get social by managerId <a name="0x028325a5e836ccddbf06de3d43b5aed45457017db1ce7ccd8fb6060835a714da"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x028325a5e836ccddbf06de3d43b5aed45457017db1ce7ccd8fb6060835a714da

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
filter[manager]		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /social <a name="0x9f4a4bfa2a6608f301079d5cfaa7bf89f0ef03e7b2e67d84c221cf10509d0ef4"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x9f4a4bfa2a6608f301079d5cfaa7bf89f0ef03e7b2e67d84c221cf10509d0ef4

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
action		// Parameter Description...
friend		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /subscriptions/{id} <a name="0xf421e49095a06121fe3c7cf4681aebfbc52d6aab1e68679aedae4465679f7051"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0xf421e49095a06121fe3c7cf4681aebfbc52d6aab1e68679aedae4465679f7051

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
id		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## Get all subscriptions for a user <a name="0x230ecae3f32982de44e83c418603d5dc3320ffb4fd3ea3381edeb834546c92e0"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x230ecae3f32982de44e83c418603d5dc3320ffb4fd3ea3381edeb834546c92e0

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
None
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /subscriptions <a name="0x277bdb41377bede649c4b4649f72b4e1b9e249f2cc9cab278f63ce0ff5f19e6a"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x277bdb41377bede649c4b4649f72b4e1b9e249f2cc9cab278f63ce0ff5f19e6a

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
cancel		// Parameter Description...
reactivate		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## Assign Tokens <a name="0xee465cc1dd4b64a64d49aa0c709371bb93ef601139b2132361c25d3a8d2d7a86"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0xee465cc1dd4b64a64d49aa0c709371bb93ef601139b2132361c25d3a8d2d7a86

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
distribution		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /assign-tokens <a name="0x6d8726ac8120e5629ce81b9f3a24f31d71dbb0b7bb8e897722e08a87ef9b32af"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x6d8726ac8120e5629ce81b9f3a24f31d71dbb0b7bb8e897722e08a87ef9b32af

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
None
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## Get Assistant Manager Settings <a name="0xefaddf4ccf2857c64d963ef29661a51b1bfd1bc32b49b9d5b1bfa4e55ca59597"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0xefaddf4ccf2857c64d963ef29661a51b1bfd1bc32b49b9d5b1bfa4e55ca59597

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
None
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /assistant-manager <a name="0xb702dd9fc5d774a8349a0c06d60b773d142b0f33602367224c680f1076c9f1f9"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0xb702dd9fc5d774a8349a0c06d60b773d142b0f33602367224c680f1076c9f1f9

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
action		// Parameter Description...
club		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /hall-of-fame/{id} <a name="0xb0002aa8688c8c699f82228f179bbb9827c080788896f70cf197bde46702a577"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0xb0002aa8688c8c699f82228f179bbb9827c080788896f70cf197bde46702a577

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
id		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /hall-of-fame <a name="0xadeaadc99b8996230e3b2b19f7cb709323cd62a57b3f5d1a87aa348720d13c8f"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0xadeaadc99b8996230e3b2b19f7cb709323cd62a57b3f5d1a87aa348720d13c8f

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
addSnapshot		// Parameter Description...
club		// Parameter Description...
playerId		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /top/{id} <a name="0x5e7288753b83ef877a736c6840d20c419acc454b0b628f6dda772cf2995a4120"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x5e7288753b83ef877a736c6840d20c419acc454b0b628f6dda772cf2995a4120

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
sortBy		// Parameter Description...
id		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## List all Unions <a name="0xfc8b049f92729eb1370b0fd8438c3bc6a21d72e3293a5c4aef1aeaa405527fbd"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0xfc8b049f92729eb1370b0fd8438c3bc6a21d72e3293a5c4aef1aeaa405527fbd

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
None
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /unions <a name="0xace6db3b78ba87d787f17f1bc6c5d67dd4693c3ee70597f3d9bda27dfe602e7f"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0xace6db3b78ba87d787f17f1bc6c5d67dd4693c3ee70597f3d9bda27dfe602e7f

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
bannerImageURL		// Parameter Description...
logoImageURL		// Parameter Description...
name		// Parameter Description...
president		// Parameter Description...
settings		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /chat-messages <a name="0x4232ea268e1b214f60f5b4e8f66a91aa882e797380647f164f9fffa5e3eb8d87"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x4232ea268e1b214f60f5b4e8f66a91aa882e797380647f164f9fffa5e3eb8d87

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
channel		// Parameter Description...
message		// Parameter Description...
senderClub		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /chat-messages/{id} <a name="0x8234f51bd417dbbc989a420a25c70fa2219a8fe7c8c5248ac354988a612fbec4"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x8234f51bd417dbbc989a420a25c70fa2219a8fe7c8c5248ac354988a612fbec4

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
channel		// Parameter Description...
endDate		// Parameter Description...
id		// Parameter Description...
limit		// Parameter Description...
startDate		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /union-history/{id} <a name="0x3b0a621aa24e342a4a1eae6d84288e1845fe08d2716b68bf74b4ff4b955d6aa1"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x3b0a621aa24e342a4a1eae6d84288e1845fe08d2716b68bf74b4ff4b955d6aa1

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
endDate		// Parameter Description...
id		// Parameter Description...
limit		// Parameter Description...
startDate		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /union-challenge-packs <a name="0x6cc166c7c43fac2f014dd7ff4bb9313a1abfe5c0ee05d08006ad889b35c8fd1c"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x6cc166c7c43fac2f014dd7ff4bb9313a1abfe5c0ee05d08006ad889b35c8fd1c

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
None
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /union-challenge-packs/{id} <a name="0x70b0877cd8e105ffe088d033681fc18b99d95e3770dc7844d3a0bb9253648821"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x70b0877cd8e105ffe088d033681fc18b99d95e3770dc7844d3a0bb9253648821

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
id		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## List chats for a manager <a name="0x4b6bb673477fb8655b387c57dabd9e233fe931ea421dc2c72dba57cc643fedb1"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0x4b6bb673477fb8655b387c57dabd9e233fe931ea421dc2c72dba57cc643fedb1

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
filter[manager]		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /chats/{id} <a name="0xa41dd6906d145acbd956e787b72b3c04a9df6249adf03ef941c5f32ba6d872b4"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0xa41dd6906d145acbd956e787b72b3c04a9df6249adf03ef941c5f32ba6d872b4

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
id		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
## /chats <a name="0xe66ced51b1d9fc1b156f55eee11f2785b8263b4de4ea5ac36cdc1ba48611931f"></a>

{{ Describe the endpoint. Explain what it does and, if possible, deep link to the Web2 documentation. }}

**Web2 Docs:** {{ URL to endpoint documentation }}

You'll need the **Endpoint ID** to call this endpoint.

**Endpoint ID:** 0xe66ced51b1d9fc1b156f55eee11f2785b8263b4de4ea5ac36cdc1ba48611931f

[Request Parameters](https://docs.api3.org/pre-alpha/protocols/request-response/request.html#request-parameters)

```solidity
managers		// Parameter Description...
message		// Parameter Description...
```

[Response](https://docs.api3.org/pre-alpha/airnode/specifications/reserved-parameters.html#path)

```json
{ Add example response json here }
```
----
